## 支持设备列表

| 主控   | 板卡型号      | 维基教程                                                     |
| ------ | ------------- | ------------------------------------------------------------ |
| RK3568 | ROC-RK3568-PC | [https://wiki.t-firefly.com/zh_CN/ROC-RK3568-PC/](https://wiki.t-firefly.com/zh_CN/ROC-RK3568-PC/) |
| RK3568 | AIO-3568J     | [https://wiki.t-firefly.com/Core-3568J/](https://wiki.t-firefly.com/Core-3568J/) |

## 编译教程

[https://wiki.t-firefly.com/zh_CN/Firefly-Linux-Guide/manual_openharmony.html](https://wiki.t-firefly.com/zh_CN/Firefly-Linux-Guide/manual_openharmony.html)
