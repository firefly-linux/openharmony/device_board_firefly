#!/bin/bash

# Copyright (c) 2021 HiHope Open Source Organization .
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

pushd ${1}
ROOT_DIR=${5}
export PRODUCT_PATH=${4}

KERNEL_SRC_TMP_PATH=${ROOT_DIR}/out/kernel/src_tmp/linux-5.10
KERNEL_SOURCE=${ROOT_DIR}/kernel/linux/linux-5.10
KERNEL_PATCH=${ROOT_DIR}/kernel/linux/patches/linux-5.10/rk3568_patch/kernel.patch
HDF_PATCH=${ROOT_DIR}/kernel/linux/patches/linux-5.10/rk3568_patch/hdf.patch

FIREFLY_KERNEL_PATCH=${ROOT_DIR}/device/hihope/rk3568/kernel/patches
FIREFLY_KERNEL_FILES=${ROOT_DIR}/device/hihope/rk3568/kernel/files

rm -rf ${KERNEL_SRC_TMP_PATH}
mkdir -p ${KERNEL_SRC_TMP_PATH}

cp -arf ${KERNEL_SOURCE}/* ${KERNEL_SRC_TMP_PATH}/

cd ${KERNEL_SRC_TMP_PATH}

#合入HDF patch
bash ${ROOT_DIR}/drivers/adapter/khdf/linux/patch_hdf.sh ${ROOT_DIR} ${KERNEL_SRC_TMP_PATH} ${HDF_PATCH}

#合入kernel patch
patch -p1 < ${KERNEL_PATCH}

#合入firefly patch
for patch in $(readlink -f  $FIREFLY_KERNEL_PATCH/*.patch)
do
	patch -p1 < ${patch}
done

#合入files
cp -rf $FIREFLY_KERNEL_FILES/* ${KERNEL_SRC_TMP_PATH}

cp -rf ${3}/kernel/logo* ${KERNEL_SRC_TMP_PATH}/

#编译内核
./make-extboot.sh ${6}

mkdir -p ${2}

cp resource.img ${2}/resource.img
cp ${3}/loader/parameter_5_10.txt ${2}/parameter.txt
cp ${3}/loader/MiniLoaderAll.bin ${2}/MiniLoaderAll.bin
cp ${3}/loader/uboot_5_10.img ${2}/uboot.img
cp ${3}/loader/config.cfg ${2}/config.cfg
popd
