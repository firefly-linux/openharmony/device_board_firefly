set -e
set -x

export PATH=../../../../prebuilts/clang/ohos/linux-x86_64/llvm/bin/:$PATH
export PRODUCT_PATH=vendor/hihope/rk3568
IMAGE_SIZE=64  # 64M
IMAGE_BLOCKS=4096

CPUs=$(nproc)
MAKE="make CROSS_COMPILE=../../../../prebuilts/gcc/linux-x86/aarch64/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-"
BUILD_PATH=boot_linux
EXTLINUX_PATH=${BUILD_PATH}/extlinux
EXTLINUX_CONF=${EXTLINUX_PATH}/extlinux.conf
KERNEL_DTB=rk-kernel.dtb
KERNEL_DEFCONFIG=rockchip_linux_defconfig

mk_configs=(
	"FIREFLY-RK3568-ROC-PC rk3568-firefly-roc-pc"
	"FIREFLY-RK3568-ROC-PC-MIPI rk3568-firefly-roc-pc-mipi_m10r800v2"
	"FIREFLY-RK3568-AIOJ rk3568-firefly-aioj"
	"FIREFLY-RK3568-AIOJ-MIPI rk3568-firefly-aioj-mipi_m10r800v2"
)

function make_image()
{
	local default_name=$1
	local dtb
	local default_dtb
	local config

	${MAKE} ARCH=arm64 ${KERNEL_DEFCONFIG}

	for config in "${mk_configs[@]}"; do
		name=$(echo $config | cut -d " " -f 1)
		dtb=$(echo $config | cut -d " " -f 2)
		if [ "$name" == "$default_name" ];then
			default_dtb="$dtb.dtb"
			break
		fi
	done

	if [ -n "$default_dtb" ];then
		${MAKE} ARCH=arm64 ${dtb}.img -j${CPUs}

		[ -d $BUILD_PATH ] && rm -rf ${BUILD_PATH}
		mkdir -p ${BUILD_PATH}
		mkdir -p ${EXTLINUX_PATH}

		# dtb
		cp -f arch/arm64/boot/dts/rockchip/*.dtb ${BUILD_PATH}

		# image
		cp -f arch/arm64/boot/Image ${BUILD_PATH}

		# default dtb
		cd ${BUILD_PATH} && ln -s $default_dtb $KERNEL_DTB && cd -

		# logo
		cp -f logo.bmp logo_kernel.bmp ${BUILD_PATH}

		# extlinux.conf
		echo -e "label rockchip-kernel-5.10" > ${EXTLINUX_CONF}
		echo -e "\tkernel /Image" >> ${EXTLINUX_CONF}
		echo -e "\tfdt /$KERNEL_DTB" >> ${EXTLINUX_CONF}
		echo -e "\tinitrd /ramdisk.img" >> ${EXTLINUX_CONF}
		echo -e "\tappend earlycon=uart8250,mmio32,0xfe660000 root=PARTUUID=614e0000-0000-4b53-8000-1d28000054a9 rw rootwait rootfstype=ext4" >> ${EXTLINUX_CONF}
		
	else
		return -1
	fi
}

make_image $@
